/*******************************************************************************
* CSE 460 - Spring 2016 - Phase III: Memory Management
* Programmer:      Raymond Aceves, Caroline Hopkins
* File name:       Assembler.h
* Date:            6/13/2016
* Description:	   Declares the Assembler class, classes for the various
* 		   instruction formats used by VirtualMachine in os.cpp, and
*                  classes for error-checking. Assembler reads a line from
*                  assembly file, translates it to corresponding binary integer
*                  , and writes object code to .o file. Any error in the
*                  assembly code file results in program closing with error
*                  message.
*******************************************************************************/
#ifndef ASSEMBLER_H
#define ASSEMBLER_H
#include <map>
#include <string>
#include <vector>
//#include <stdexcept>	//for runtime_error

using namespace std;

// binary format for instr without ADDR or CONST arguments
class Unused_form
{
public:
    unsigned UNUSED:6;
    unsigned RS:2;
    unsigned I:1;
    unsigned RD:2;
    unsigned OP:5;
};

// format for instr with ADDR argument
class Addr_form
{
public:
    unsigned ADDR:8;
    unsigned I:1;
    unsigned RD:2;
    unsigned OP:5;
};

// format for instr with CONST argument
class Const_form
{
public:
    int CONST:8;
    unsigned I:1;
    unsigned RD:2;
    unsigned OP:5;
};

//Classes for error-checking

/*class OutOfRangeException: public runtime_error {
public:
    OutOfRangeException(): runtime_error("Out of range error on line ") { }
};

class WordsException: public runtime_error {
public:
    WordsException(): runtime_error("Wrong number of words on line ") { }
};*/

class Assembler
{
    typedef void(Assembler::*fp)(const vector<string> &);
public:
    Assembler();
    void assemble(ifstream &, ofstream &);
private:
    union instruction {		//union for classifying which type of format
		int binary_instruction;
		Unused_form uf;
		Addr_form af;
		Const_form cf;
	};
    instruction ins;

    int imm;
    int op1;
    int op2;
    int op3;
    int op4;
    int op5;
    int line_number;

    map<string, fp> opcode;

    int assemble_line(const vector<string> &);
    void assemble1(const vector<string> &);
    void assemble2(const vector<string> &);
    void assemble3(const vector<string> &);
    void assemble4(const vector<string> &);
    void assemble5(const vector<string> &);
    void assemble6(const vector<string> &);

    void load(const vector<string> &);
    void loadi(const vector<string> &);
    void store(const vector<string> &);
    void add(const vector<string> &);
    void addi(const vector<string> &);
    void addc(const vector<string> &);
    void addci(const vector<string> &);
    void sub(const vector<string> &);
    void subi(const vector<string> &);
    void subc(const vector<string> &);
    void subci(const vector<string> &);
    void and_f(const vector<string> &);
    void andi(const vector<string> &);
    void xor_f(const vector<string> &);
    void xori(const vector<string> &);
    void compl_f(const vector<string> &);
    void shl(const vector<string> &);
    void shla(const vector<string> &);
    void shr(const vector<string> &);
    void shra(const vector<string> &);
    void compr(const vector<string> &);
    void compri(const vector<string> &);
    void getstat(const vector<string> &);
    void putstat(const vector<string> &);
    void jump(const vector<string> &);
    void jumpl(const vector<string> &);
    void jumpe(const vector<string> &);
    void jumpg(const vector<string> &);
    void call(const vector<string> &);
    void return_f(const vector<string> &);
    void read(const vector<string> &);
    void write(const vector<string> &);
    void halt(const vector<string> &);
    void noop(const vector<string> &);
    void invalid();
};



#endif
