/*******************************************************************************
* CSE 460 - Spring 2016 - Phase III: Memory Management
* Programmer:      Raymond Aceves, Caroline Hopkins
* File name:       Assembler.cpp
* Date:            6/13/2016
* Description:     Implementation of Assembler.h. Assembler translates each line
* 		   of assembly code into its binary representation and ouputs
* 		   to .o file to be used by VirtualMachine in os.cpp
*******************************************************************************/
#include "Assembler.h"
#include <map>
#include <string>
#include <cstring> //strlen()
#include <vector>
#include <fstream> //ifstream, ofstream
#include <iomanip>
#include <stdlib.h> //atoi()
using namespace std;

void split(const string& text, vector<string>& words)
//this function is mostly from timothy budd's Data Structures book
//this function will split a string into a vector of words
//modified to ignore anything after and including '!'
//modified to not require separators as a parameter
{
    int length = text.length();
    int start = text.find_first_not_of("\t ", 0);

    while((start >= 0) && (start < length) && (text[start] != '!')) {
        int stop = text.find_first_of("\t !", start);
        if((stop < 0) || (stop > length))
            stop = length;
        words.push_back(text.substr(start, stop-start));
        start = text.find_first_not_of("\t ", stop);
    }

} //split()

//fills the function pointer map.
Assembler::Assembler()
{
    line_number = 0;
    opcode["load"] = &Assembler::load;
    opcode["loadi"] = &Assembler::loadi;
    opcode["store"] = &Assembler::store;
    opcode["add"] = &Assembler::add;
    opcode["addi"] = &Assembler::addi;
    opcode["addc"] = &Assembler::addc;
    opcode["addci"] = &Assembler::addci;
    opcode["sub"] = &Assembler::sub;
    opcode["subi"] = &Assembler::subi;
    opcode["subc"] = &Assembler::subc;
    opcode["subci"] = &Assembler::subci;
    opcode["and"] = &Assembler::and_f;
    opcode["andi"] = &Assembler::andi;
    opcode["xor"] = &Assembler::xor_f;
    opcode["xori"] = &Assembler::xori;
    opcode["compl"] = &Assembler::compl_f;
    opcode["shl"] = &Assembler::shl;
    opcode["shla"] = &Assembler::shla;
    opcode["shr"] = &Assembler::shr;
    opcode["shra"] = &Assembler::shra;
    opcode["compr"] = &Assembler::compr;
    opcode["compri"] = &Assembler::compri;
    opcode["getstat"] = &Assembler::getstat;
    opcode["putstat"] = &Assembler::putstat;
    opcode["jump"] = &Assembler::jump;
    opcode["jumpl"] = &Assembler::jumpl;
    opcode["jumpe"] = &Assembler::jumpe;
    opcode["jumpg"] = &Assembler::jumpg;
    opcode["call"] = &Assembler::call;
    opcode["return"] = &Assembler::return_f;
    opcode["read"] = &Assembler::read;
    opcode["write"] = &Assembler::write;
    opcode["halt"] = &Assembler::halt;
    opcode["noop"] = &Assembler::noop;
} //Assembler()

void Assembler::assemble(ifstream & s_file, ofstream & o_file)
{
    // split each line into words vector using split()
    string current_line;
    getline(s_file, current_line);

    while(!s_file.eof()){
        line_number++;
        vector<string> words;
        split(current_line, words);

        if (words.size() == 0) {
            getline(s_file, current_line);
        } else {
            //translate non-empty line into object code, write to bin_file
            o_file << setfill('0') << setw(5) << assemble_line(words);
            getline(s_file, current_line);
            if (!s_file.eof())
                o_file << endl;
        }

    }

} //assemble()

//translates line into integer to be written to object file
int Assembler::assemble_line(const vector<string> & words)
{
    ins.binary_instruction = 0;
    if (!opcode[words[0]])
        invalid();	//If opcode is not in table, set OP to 31
    else
        (this->*opcode[words[0]])(words);	//call function from fp map
   
    return ins.binary_instruction;
} //

//the following assembleX functions set bit fields

//OP RD RS format
void Assembler::assemble1(const vector<string> & v)
{
    //Check that registers are between 0 and 3 and word count == 3
    if (v.size() != 3)
        invalid();
    else {
        int r1 = atoi(v[1].c_str());
        int r2 = atoi(v[2].c_str());
        if (r1 > 3 || r1 < 0 || r2 > 3 || r2 < 0)
            invalid();
        else {
            ins.uf.RD = r1;
            ins.uf.RS = r2;
        }
    }
}

//OP RD ADDR format
void Assembler::assemble2(const vector<string> & v)
{
    //Check that register is between 0 and 3,
    //check that address is between 0 and 255,
    //and check that word count == 3
    if (v.size() != 3)
        invalid();
    else {
        int r = atoi(v[1].c_str());
        int a = atoi(v[2].c_str());
        if (r < 0 || r > 3 || a < 0 || a > 255)
            invalid();
        else {
            ins.af.RD = r;
            ins.af.ADDR = a;
        }
    }
}

//OP RD 1 CONST format
void Assembler::assemble3(const vector<string> & v)
{
    //Check that reg is between 0 and 3,
    //check that constant is between -128 and 127
    //and check that word count == 3
    if (v.size() != 3)
        invalid();
    else {
        int r = atoi(v[1].c_str());
        int c = atoi(v[2].c_str());
        if (r < 0 || r > 3 || c < -128 || c > 127)
            invalid();
        else {
            ins.cf.RD = r;
            ins.cf.I = 1;
            ins.cf.CONST = c;
        }
    }
}

//OP RD format
void Assembler::assemble4(const vector<string> & v)
{   //Check that reg is between 0 and 3 and word count == 2

    if (v.size() != 2)
        ins.uf.OP = 31;
    else {
        int r = atoi(v[1].c_str());
        if (r < 0 || r > 3)
            ins.uf.OP = 31;
        else
            ins.uf.RD = r;
    }
}

//OP 1 ADDR format
void Assembler::assemble5(const vector<string> & v)
{
    //Check that address is between 0 and 255 and word count == 2
    if (v.size() != 2)
        invalid();
    else {
        int a = atoi(v[1].c_str());
        ins.af.I = 1;
        ins.af.ADDR = a;            
    }
}

//"no parameter" format (halt & noop)
void Assembler::assemble6(const vector<string> & v)
{
    //Check that no extra words in line
    if (v.size() != 1)
        invalid();
}

//functions for each opcode. sets the opcode and I (if appropriate) bit fields,
//calls appropriate assembleX instruction according to instr format.

void Assembler::load(const vector<string> & v)
{
    ins.af.OP = 0;
    assemble2(v);
}

void Assembler::loadi(const vector<string> & v)
{
    ins.cf.OP = 0;
    assemble3(v);
}

void Assembler::store(const vector<string> & v)
{
    ins.af.OP = 1;
    ins.af.I = 1;
    assemble2(v);
}

void Assembler::add(const vector<string> & v)
{
    ins.uf.OP = 2;
    assemble1(v);
}

void Assembler::addi(const vector<string> & v)
{
    ins.cf.OP= 2;
    assemble3(v);
}

void Assembler::addc(const vector<string> & v)
{
    ins.uf.OP = 3;
    assemble1(v);
}

void Assembler::addci(const vector<string> & v)
{
    ins.cf.OP = 3;
    assemble3(v);
}

void Assembler::sub(const vector<string> & v)
{
    ins.uf.OP = 4;
    assemble1(v);
}

void Assembler::subi(const vector<string> & v)
{
    ins.cf.OP = 4;
    assemble3(v);
}

void Assembler::subc(const vector<string> & v)
{
    ins.uf.OP = 5;
    assemble1(v);
}

void Assembler::subci(const vector<string> & v)
{
    ins.cf.OP = 5;
    assemble3(v);
}

void Assembler::and_f(const vector<string> & v)
{
    ins.uf.OP = 6;
    assemble1(v);
}

void Assembler::andi(const vector<string> & v)
{
    ins.cf.OP = 6;
    assemble3(v);
}

void Assembler::xor_f(const vector<string> & v)
{
    ins.uf.OP = 7;
    assemble1(v);
}

void Assembler::xori(const vector<string> & v)
{
    ins.cf.OP = 7;
    assemble3(v);
}

void Assembler::compl_f(const vector<string> & v)
{
    ins.uf.OP = 8;
    assemble4(v);
}

void Assembler::shl(const vector<string> & v)
{
    ins.uf.OP = 9;
    assemble4(v);
}

void Assembler::shla(const vector<string> & v)
{
    ins.uf.OP = 10;
    assemble4(v);
}

void Assembler::shr(const vector<string> & v)
{
    ins.uf.OP = 11;
    assemble4(v);
}

void Assembler::shra(const vector<string> & v)
{
    ins.uf.OP = 12;
    assemble4(v);
}

void Assembler::compr(const vector<string> & v)
{
    ins.uf.OP = 13;
    assemble1(v);
}

void Assembler::compri(const vector<string> & v)
{
    ins.cf.OP = 13;
    assemble3(v);
}

void Assembler::getstat(const vector<string> & v)
{
    ins.uf.OP = 14;
    assemble4(v);
}

void Assembler::putstat(const vector<string> & v)
{
    ins.uf.OP = 15;
    assemble4(v);
}

void Assembler::jump(const vector<string> & v)
{
    ins.af.OP = 16;
    assemble5(v);
}

void Assembler::jumpl(const vector<string> & v)
{
    ins.af.OP = 17;
    assemble5(v);
}

void Assembler::jumpe(const vector<string> & v)
{
    ins.af.OP = 18;
    assemble5(v);
}

void Assembler::jumpg(const vector<string> & v)
{
    ins.af.OP = 19;
    assemble5(v);
}

void Assembler::call(const vector<string> & v)
{
    ins.af.OP = 20;
    assemble5(v);
}

void Assembler::return_f(const vector<string> & v)
{
    ins.uf.OP = 21;
    assemble6(v);
}

void Assembler::read(const vector<string> & v)
{
    ins.uf.OP = 22;
    assemble4(v);
}

void Assembler::write(const vector<string> & v)
{
    ins.uf.OP = 23;
    assemble4(v);
}

void Assembler::halt(const vector<string> & v)
{
    ins.uf.OP = 24;
    assemble6(v);
}

void Assembler::noop(const vector<string> & v)
{
    ins.uf.OP = 25;
    assemble6(v);
}

void Assembler::invalid()
{
    ins.uf.OP = 31;
}
