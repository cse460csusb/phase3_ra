/********************************************************************************
* CSE 460 - Spring 2016 - Phase III: Memory Management
* Programmer:      Raymond Aceves, Caroline Hopkins
* File name:       VirtualMachine.h
* Date:            6/13/2016
* Description:     VirtualMachine is a representation of a machine with 4
* 		           general purpose registers, a program counter, instruction
* 		           register, status register, stack pointer, clock, ALU, and
* 		           256 word memory. VirtualMachine reads in object file
* 		           created by Assembler and executes the instructions. Outputs
* 		           results to .out file.
********************************************************************************/
#ifndef VIRTUALMACHINE_H
#define VIRTUALMACHINE_H
#include <vector>
#include <string>
#include <map>
#include <fstream>
#include <stdexcept> //for runtime_error
#include "page_table.h"

using namespace std;

//Error-checking
class OutOfRange: public runtime_error {
public:
    OutOfRange(): runtime_error("Out of range error in input file line ") {}
};

//used in union below for parsing .o file
class Unused_form_vm
{
public:
    unsigned UNUSED:6;
    unsigned RS:2;
    unsigned I:1;
    unsigned RD:2;
    unsigned OP:5;
};

//used in union below for parsing .o file
class Addr_form_vm
{
public:
    unsigned ADDR:8;
    unsigned I:1;
    unsigned RD:2;
    unsigned OP:5;
};

//used in union below for parsing .o file
class Const_form_vm
{
public:
    int CONST:8;
    unsigned I:1;
    unsigned RD:2;
    unsigned OP:5;
};

class Status_Reg
{
public:
    unsigned carry:1;
    unsigned greater:1;
    unsigned equal:1;
    unsigned less:1;
    unsigned overflow:1;
    unsigned vm_return:3;
    unsigned io_reg:2;
    unsigned PF:1;
};

class VirtualMachine
{

    typedef void(VirtualMachine::*fp)();
public:
    VirtualMachine();
    void run();
private:
    //used in decode() for parsing integers from .o file
    union instruction {
        int binary_instruction;
        Unused_form_vm uf;
        Addr_form_vm af;
        Const_form_vm cf;
    };
    instruction ins;

    union Status{
        int full;
	Status_Reg split;
    };
    Status sr;

    map<int, fp> opcode;

    static const int REG_FILE_SIZE = 4; //number of registers
    static const int MEM_SIZE = 256;    //size of memory
    static const int TIME_SLICE = 15;
    vector<int> r;
    vector<int> mem;
    int pc;
    int fault;
    int ir;
    int sp;
    int clock;
    int base;
    int limit;
    int all_limit;
    string in_file;
    string out_file;
    unsigned OP, RD, I, RS, ADDR;
    int CONST;
    int largest_stack;

    int store_in_16(const int &);
    int use_all_32(const int &);
    void load(fstream &, const int &, const int&); //phase 3 load
    //void load(ifstream &, const int &);
    void fetch_decode_execute();
    void decode();
    void execute();

    void push();
    void pop();

    int addition(const int &, const int &, const int &);

    void load();
    void loadi();
    void store();
    void add();
    void addi();
    void addc();
    void addci();
    void sub();
    void subi();
    void subc();
    void subci();
    void and_f();
    void andi();
    void xor_f();
    void xori();
    void compl_f();
    void shl();
    void shla();
    void shr();
    void shra();
    void compr();
    void compri();
    void getstat();
    void putstat();
    void jump();
    void jumpl();
    void jumpe();
    void jumpg();
    void call();
    void return_f();
    void read();
    void write();
    void halt();
    void noop();

    friend class OS;

    vector<int> frame_registers;
    vector<int> limits;
    PageTable TLB;
    int current_page;
    void write_to_file(fstream&, const int &, const int &);
    int debug;
    int mem_ref;
};

#endif
