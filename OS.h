/********************************************************************************
* CSE 460 - Spring 2016 - Phase III: Memory Management
* Programmer:      Raymond Aceves, Caroline Hopkins
* File name:       OS.h
* Date:            6/13/16
* Description:     OS represents a rudimentary operating system. Creates instances
*                  of the Assembler and VirtualMachine classes. Reads in .s file,
*                  Assembler translates to .o file, then VirtualMachine executes
*                  the instructions in the .o file. Any input to the running
*                  program should be contained in a .in file of the same name.
*                  Output is written to .out file.
********************************************************************************/
#ifndef OS_H
#define OS_H

#include <list>
#include <queue>
#include <vector>
#include <fstream>
#include <map>
#include "VirtualMachine.h"
#include "page_table.h"
using namespace std;


//Program Control Block. Adding P to beginning of variable names so no confusion.
//Contains PC, status register, stack pointer, registers, base, limit, and clock
//of a process.
class PCB
{
private:
    PCB(string &);
    static const int REG_SIZE = 4;
    int pc, sp, sr, base, limit;
    vector<int> r;
    string process_name;
    string st_file;//need to change this implementation to fstream
    ofstream out_file;	
    ifstream in_file;			//filestreams
    fstream o_file;
    int CPU_time, wait_time, turn_time, IO_time, lrg_st_size;		//accounting info
    int ready_time;	//used for waiting in IO queue.
    int this_time; //used to keep track of when pcb enetered current state

    //phase 3 additives
    PageTable table;
    int pid;
    int PFs;
    int nPFs;

    friend class OS;
};

class OS 
{
    typedef void(OS::*fp)();
public:
    OS();
    void run(char*);
private:
    ifstream prog_instream;
    ofstream prog_out;

    void load();
    void read(int);
    void write(int);
    void scheduler();
    void context();
   
    void next_running();
    void save_running(); 
    void time_slice();
    void halt_instr();
    void out_of_bound();
    void stack_overflow();
    void stack_underflow();
    void invalid_op();
    void read_op();
    void write_op();
    void overflow();
    void page_fault();
    int choose_victim();

    void accounting();
    void accounting_final();

    map<int, fp> vm_return;
    
    VirtualMachine v;
    list<PCB *> jobs, completed;
    
    queue<PCB *> readyQ, waitQ;
    PCB * running;

    int idle_time;
    int context_time;

    //phase 3 additives
    InvertedTable inverted_table;
    queue<int> empty_frames;
    queue<int> frame_registersQ;
    list<PCB *> more_jobs;
    int replacement_alg; //0=FIFO, 1=LRU
    void remove_page(const int &);
    void write_to_stack();
};

#endif
