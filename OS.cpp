/********************************************************************************
* CSE 460 - Spring 2016 - Phase III: Memory Management
* Programmer:      Raymond Aceves, Caroline Hopkins
* File name:       OS.cpp
* Date:            6/13/2016
* Description:     OS represents a rudimentary operating system. Creates instances
*                  of the Assembler and VirtualMachine classes. Reads in .s file,
*                  Assembler translates to .o file, then VirtualMachine executes
*                  the instructions in the .o file. Any input to the running
*                  program should be contained in a .in file of the same name.
*                  Output is written to .out file.
* 
********************************************************************************/
#include "Assembler.h"
#include "VirtualMachine.h"
#include "OS.h"
#include <fstream>
#include <string>
#include <iostream>
#include <stdlib.h> //exit()
#include <iomanip> 	//for setprecision
#include <string.h> //strcmp()
using namespace std;

//initialize variables, open file streams.
PCB::PCB(string & prog_name) 
{
    prog_name.resize(prog_name.length()-2); //process_name is title only, no ".*"
    process_name = prog_name;
    pc = sr = base = limit = 0;
    PFs = nPFs = 0;
    sp = 256;
    r = vector<int>(REG_SIZE);
    for(int i = 0; i < r.size(); ++i)
        r[i] = 0;

    CPU_time= wait_time= turn_time= IO_time= lrg_st_size= ready_time= 0; 
    this_time = 0;
    
    //open .o stream
    prog_name = process_name + ".o";
    o_file.open(prog_name.c_str());	
    if (!o_file.is_open()) {  //Check for error when opening
        cout << "Unable to open .o file" << endl;
        exit(1);
    }

    //open .in stream
    prog_name = process_name + ".in";
    in_file.open(prog_name.c_str());	
    if (!in_file.is_open()) {  //Check for error when opening
        cout << "Unable to open .in file" << endl;
        exit(1);
    }

    //open .out stream
    prog_name = process_name + ".out";
    out_file.open(prog_name.c_str());	
    if (!out_file.is_open()) { //Check for error when opening
        cout << "Unable to open .out file" << endl;
        exit(1);
    }
    
    //set .st file name
    st_file = process_name + ".st";

    //create .st file for stack use
    string temp = "touch " + st_file;
    system(temp.c_str());

}//PCB constructor

OS::OS()
{
    idle_time = context_time = 0;
    vm_return[0] = &OS::time_slice;
    vm_return[1] = &OS::halt_instr;
    vm_return[2] = &OS::out_of_bound;
    vm_return[3] = &OS::stack_overflow;
    vm_return[4] = &OS::stack_underflow;
    vm_return[5] = &OS::invalid_op;
    vm_return[6] = &OS::read_op;
    vm_return[7] = &OS::write_op;

    system("ls *.s > progs");   //get progs

    //open progs stream for program names
    prog_instream.open("progs");       //read prog file names from file
    if (!prog_instream.is_open()) {
        cout << "Unable to open progs" << endl;
        exit(1);
    }

    //this opens progs.out for our own verification of cpu sharing
    prog_out.open("progs.out");
    if (!prog_out.is_open()) {
        cout << "Unable to open progs.out" << endl;
        exit(1);
    }


    //phase3
    for(int i = 0; i < 32; i++)
        empty_frames.push(i);
}//OS()

void OS::load()
{
    // declaring variables
    string filename;
    Assembler a;
    int pids = 1;

    //loop for initializing each program to be run.
    while (prog_instream >> filename) {
        ifstream s_instream(filename.c_str());//open .s file for asembler to read
	
        if(!s_instream.is_open()) { //check that .s file opened properly
            cout << "Unable to open .s file" << filename << endl;
            exit(1);
        }

        //rename .s to .o
	filename.resize(filename.length()-1);	
	filename = filename + "o";

	ofstream o_outstream(filename.c_str());	//open .o file for assembler to output
        if(!o_outstream.is_open()) {
            cout << "Unable to open " << filename << endl;
            exit(1);
        }

        //assemble the .s file to create object .o file
	a.assemble(s_instream, o_outstream);
	
        //close both files
	s_instream.close();
	o_outstream.close();
	
	//create a PCB for each process being loaded.
        //set initial pc,sp, base and limit for each PCB
	PCB *p = new PCB(filename);
        p->pid = pids++;
	p->pc = p->base = 0;
        //find program limit
        p->o_file.seekg(0, ios::end); 
        p->limit = p->o_file.tellg();
        p->limit = (p->limit+1) / 6;

        if(readyQ.size() < 5){
            jobs.push_back(p);
	    readyQ.push(p);	//at the beginning, 5 jobs are added to Ready Queue
            v.load(p->o_file, 0, empty_frames.front());//load first page of .o file into a free frame
            p->PFs++;
            p->table.entry[0].frame = empty_frames.front();//set frame value in page table for page 0
            p->table.entry[0].valid = 1;
            p->table.entry[0].modify = 0;
            inverted_table.entry[empty_frames.front()].pid = p->pid;//set pid/page value in inverted page table for frame x
            inverted_table.entry[empty_frames.front()].page = 0;//set pid/page value in inverted page table for frame x
            inverted_table.entry[empty_frames.front()].valid = 1; //set valid bit for above entry to 1
            frame_registersQ.push(empty_frames.front()); //for FIFO
            empty_frames.pop();
            v.all_limit += 8;
        } else
            more_jobs.push_back(p);
    }

    //set first program as running
    running = readyQ.front();
    readyQ.pop();
    prog_instream.close(); //close "progs" file
} //load()

void OS::run(char* alg) 
{
    load();

    //set virtualMachine values for first program
    v.pc = running->table.entry[running->pc/8].frame*8 + running->pc%8;
    v.sr.full = running->sr;
    v.sp = running->sp;
    v.base = running->table.entry[running->pc/8].frame*8;
    v.limit = v.limits[v.base/8];
    v.TLB = running->table;
    v.current_page = running->pc/8;

    //set page replacement algorithm variable
    if(!strcmp(alg, "-fifo"))
        replacement_alg = 0;
    else if(!strcmp(alg, "-lru"))
        replacement_alg = 1;
    else {
        cout << alg << " is an invalid argument\n";
        exit(1);
    } 
    
    while(jobs.size() > 0){ //continue until all programs have been terminated
        //Run the process
        running->this_time = v.clock;//save the time before running
        //check for page fault
        v.run();
        running->nPFs += v.mem_ref;
        //set the CPU time when program gets kicked off of the CPU
        running->CPU_time += (v.clock - running->this_time);
        //output to progs.out for time sharing verification
        prog_out<< running->process_name <<endl << (v.clock - running->this_time) <<" ticks\n";


        //trap has occurred, need to rearange queues and see why cpu stopped
        scheduler();
        context();
    }

    accounting_final(); //adds system information to all .out files
    prog_out.close();   //close progs.out
}//run()

//will check waitQ and move items to readyQ if necessary/ready
void OS::scheduler()
{
    while((jobs.size() < 5) and (more_jobs.size() > 0)){
        int victim = choose_victim();
        jobs.push_back(more_jobs.front());
        readyQ.push(more_jobs.front());
        v.load(more_jobs.front()->o_file, 0, victim);//load first page of .o file into a free frame
        more_jobs.front()->PFs++;
        more_jobs.front()->table.entry[0].modify = 0;
        more_jobs.front()->table.entry[0].frame = victim;//set frame value in page table for page 0
        more_jobs.front()->table.entry[0].valid = 1;
        inverted_table.entry[victim].pid = more_jobs.front()->pid;//set pid/page value in inverted page table for frame x
        inverted_table.entry[victim].page = 0;//set pid/page value in inverted page table for frame x
        inverted_table.entry[victim].valid = 1; //set valid bit for above entry to 1
        more_jobs.pop_front();
    } 
    //if waitQ is empty then do nothing in this module
    if(waitQ.size() == 0)
        return;
    while(1){//continue until break occurs
        //move from waitQ to readyQ if clock >= ready_time
        if((waitQ.size() > 0) and (waitQ.front()->ready_time <= v.clock)){
            readyQ.push(waitQ.front());
            waitQ.front()->IO_time += (v.clock - (waitQ.front()->this_time));	//IO time
            waitQ.front()->this_time = v.clock;		//set up for wait time
            waitQ.pop();
        } else
            break;
    } 

    //if nothing is in readyQ but there are still jobs in waitQ
    //then cpu is idling until a job from waitQ has moved to readyQ
    if((readyQ.size() == 0) and (waitQ.size() > 0)){
        idle_time += (waitQ.front()->ready_time - v.clock);
        v.clock = waitQ.front()->ready_time;
        scheduler();
    }
} //scheduler()


void OS::context()
{
    write_to_stack();

    //increment clock and context time by 5
    context_time += 5;
    v.clock += 5;

    //run function that corresponds to vm_return value
    (this->*vm_return[v.sr.split.vm_return])();

 
}//context()

void OS::write_to_stack()
{
    //open current .st file as output
    ofstream st_out(running->st_file.c_str());
    if(!st_out.is_open()) { //check that .st file opened properly
        cout << "Unable to open .st file\n";
        exit(1);
    }

    //check sp to see if a stack must be written into .st
    //if sp == 256 then there is nothing to save
    if(v.sp >= v.MEM_SIZE);
    //else write the stack from VM ont the .st file
    else if(v.sp < v.MEM_SIZE){
        for(int i = v.MEM_SIZE-1; i >= v.sp; --i)
            st_out << v.mem[i]<<endl;
    }
    //close current .st
    st_out.close();
}

//will remove a page from specified frame
void OS::remove_page(const int & frame)
{

   PCB* temp_pointer;
    int temp_pid;
    int temp_page;
    if(inverted_table.entry[frame].valid){
        temp_pid = inverted_table.entry[frame].pid;
        list<PCB*>::iterator itr = jobs.begin();
        for(; itr !=jobs.end(); itr++)
            if(temp_pid == (*itr)->pid){
                temp_pointer = (*itr);
                temp_page = inverted_table.entry[frame].page;
                break;
            }
        if(itr != jobs.end()){
            if((temp_pointer->table.entry[temp_page].valid) and (temp_pointer->table.entry[temp_page].modify))
                v.write_to_file(temp_pointer->o_file, temp_page, frame);
            temp_pointer->table.entry[temp_page].valid = 0;
       }
        inverted_table.entry[frame].valid = 0;
    }
} //remove_page()

//moves the next PCB from the front of readyQ to running
//sets the CPU registers according to PCB
void OS::next_running()
{
    while((jobs.size() < 5) and (more_jobs.size() > 0)){
        int victim = choose_victim();
        jobs.push_back(more_jobs.front());
        readyQ.push(more_jobs.front());
        v.load(more_jobs.front()->o_file, 0, victim);//load first page of .o file into a free frame
        more_jobs.front()->PFs++;
        more_jobs.front()->table.entry[0].modify = 0;
        more_jobs.front()->table.entry[0].frame = victim;//set frame value in page table for page 0
        more_jobs.front()->table.entry[0].valid = 1;
        inverted_table.entry[victim].pid = more_jobs.front()->pid;//set pid/page value in inverted page table for frame x
        inverted_table.entry[victim].page = 0;//set pid/page value in inverted page table for frame x
        inverted_table.entry[victim].valid = 1; //set valid bit for above entry to 1
        more_jobs.pop_front();
    } 

    running = readyQ.front();
    readyQ.pop();
    running->wait_time += (v.clock - (running->this_time));     //wait time

    v.r[0] = running->r[0];
    v.r[1] = running->r[1];
    v.r[2] = running->r[2];
    v.r[3] = running->r[3];
    v.current_page = running->pc/8;
    v.sr.full = running->sr;
    v.sp = running->sp;


    v.largest_stack = running->lrg_st_size;
    running->this_time = v.clock;
    v.TLB = running->table;
    ifstream st_in(running->st_file.c_str());
    if(!st_in.is_open()) { //check that .st file opened properly
        cout << "Unable to open .st file here\n";
        exit(1);
    }
    
    //if sp <256 then we need to build the stack from .st file
    if(v.sp < v.MEM_SIZE){
        while(v.sp < v.all_limit){//need to make room in memory for incoming stack
            v.all_limit -= 8;
            remove_page(v.all_limit/8);
        }
        for(int i = (v.MEM_SIZE - 1); i >= v.sp; --i){
            if(st_in.eof()){
                cout<<"Error: stack recovery failed\n";
                exit(1);
            }
            st_in >> v.mem[i];
        }
    }    
    st_in.close();

    //check to see if the current page is in memory
    //if not then v.pc, v.base, v.limit, and v.current_page are not valid
    //page fault occurs
    if(running->table.entry[running->pc/8].valid == 0){
        int temp_pc = running->pc;
        PCB* temp_pointer = running;
        v.pc = 1; //set to dummy variables for the paugefault will load correctly after context();
        v.base = 0;
        v.limit = 8;
        v.sr.split.vm_return = 0;
        v.sr.split.io_reg = 1;
        v.sr.split.PF = 1;
        v.fault = temp_pc;
        running->this_time = v.clock;
        scheduler();
        context();
        temp_pointer->pc = temp_pc;
    }

    v.pc = running->table.entry[running->pc/8].frame*8 + running->pc%8;
    v.base = running->table.entry[running->pc/8].frame*8;
    v.limit = v.limits[v.base/8];
  
} //next_running()

//saves the values of the registers from CPU onto the process PCB
void OS::save_running()
{
    running->r[0] = v.r[0];
    running->r[1] = v.r[1];
    running->r[2] = v.r[2];
    running->r[3] = v.r[3];
    
    if( (v.pc == (v.base+v.limit)) and ((v.limit%8)== 0))
    //we got here because the pc got incremented to the next invalid page
    //but the line of execution caused a trap in the VM
        running->pc = (v.current_page + 1) * 8;
    else
        running->pc = (inverted_table.entry[v.pc/8].page*8) + (v.pc%8);


    running->sr = v.sr.full;
    running->sp = v.sp;
    running->this_time = v.clock;
    running->lrg_st_size = v.largest_stack;
    running->table = v.TLB;
} //save_running()

//hit when vm_return = 0
void OS::time_slice()
{
    //check if overflow was set
    if(v.sr.split.overflow)
        overflow();
    else if(v.sr.split.PF)
        page_fault();
    else{
        prog_out<<"time slice\n\n"; //for timesharing verification
        save_running();
        readyQ.push(running); //move from running to readyQ
        next_running();  //load next program from readyQ to running
    }
} //time_slice()

//when vm_return = 1
void OS::halt_instr()
{
    prog_out<<"****** halt ******\n\n";
    accounting(); //print process accounting info to .out file

    jobs.remove(running);//remove from jobs
    completed.push_back(running);//add to completed

    if((jobs.size()+more_jobs.size()) > 0){//if there are still jobs to be ran, then load next process

        next_running();
    }
} //halt_instr()

//if vm_return = 2
void OS::out_of_bound()
{
    prog_out<<"**** out of bound ****\n\n";

    running->out_file<<"Error: Out of bound reference to line "<<(v.pc)<<endl<<endl;
    accounting();

    jobs.remove(running);
    completed.push_back(running);

    if((jobs.size()+more_jobs.size()) > 0)
        next_running();
} //out_of_bound()

//if vm_return = 3
void OS::stack_overflow()
{
    if(v.all_limit > 8){//use up to 31 frames for the stack. at least one frame needed for processing
        PCB* temp_pointer;
        int temp_page;
        v.all_limit -= 8;

        list<PCB*>::iterator itr = jobs.begin();
        for(;itr != jobs.end(); itr++)
            if((*itr)->pid == inverted_table.entry[(v.sp-6)/8].pid){
                temp_pointer = (*itr);
                temp_page = inverted_table.entry[(v.sp-6)/8].page;
                break;
            }   

        if((itr != jobs.end()) and (inverted_table.entry[(v.sp-6)/8].valid ==1) and (temp_pointer->table.entry[temp_page].valid == 1)){
            if(temp_pointer->table.entry[temp_page].modify == 1){
                v.write_to_file(temp_pointer->o_file, temp_page, (v.sp-6)/8);
            }
            temp_pointer->table.entry[temp_page].valid = 0;
        }
        inverted_table.entry[(v.sp-6)/8].valid = 0;
        v.pc = v.fault;
        v.push();
        v.sr.split.vm_return = 0; 
        v.pc = v.TLB.entry[v.ADDR/8].frame*8 + (v.ADDR%8);
        save_running();
	write_to_stack();
        readyQ.push(running);
    } else {
        prog_out<<"**** stack overflow ****\n\n";

        running->out_file<<"Error: Stack Overflow has occurred.\n\n";
        accounting();

        jobs.remove(running);
        completed.push_back(running);
    }
    if((jobs.size()+more_jobs.size())>0)
        next_running();
} //stack_overflow()

//if vm_return = 4
void OS::stack_underflow()
{
    prog_out<<"**** stack underflow ****\n\n";

    running->out_file<<"Error: Stack Underflow has occurred.\n\n";
    accounting();

    jobs.remove(running);
    completed.push_back(running);

    if((jobs.size()+more_jobs.size())>0)
        next_running();
} //stack_underflow()

//if vm_return = 5
void OS::invalid_op()
{
    prog_out<<"**** invalid OP ****\n\n";

    running->out_file<<"Error: Invalid Opcode or format at line "<<(v.pc-v.base-1)<<endl<<endl;
    accounting();

    jobs.remove(running);
    completed.push_back(running);

    if((jobs.size()+more_jobs.size())>0)
        next_running();
} //invalid_op()

//if vm_return = 6
void OS::read_op()
{
    prog_out<<"I/O - read\n\n";

    save_running(); //save values from CPU to PCB

    read(v.sr.split.io_reg); //complete read operation
    
    running->ready_time = v.clock + 27;
    waitQ.push(running); //push onto back of waitQ

    if(readyQ.size() > 0)
        next_running();
    else{ //if nothing is ready then there is idle time, call scheduler
        scheduler();
        next_running();
    }
} //read_op()

//if_vm_return = 7
void OS::write_op()
{
    prog_out<<"I/O - write\n\n";

    save_running(); //save registers onto PCB

    write(v.sr.split.io_reg); //complete write operation

    running->ready_time = v.clock + 27; 
    waitQ.push(running); //push onto back of waitQ

    if(readyQ.size() > 0)
        next_running();
    else{ //if nothing is ready then there is idle time, call scheduler
        scheduler();
        next_running();
    }
}//write_op()

//completes read operation
void OS::read(int dest)  
{
    running->in_file >> running->r[dest];
    running->r[dest] = v.store_in_16(running->r[dest]);

} //read()

//completes write operation
void OS::write(int source)
{
    running->out_file << v.use_all_32(running->r[source])<<endl;
} //write()

//if vm_return = 0 but overflow bit has been set
void OS::overflow()
{
    prog_out<<"**** overflow bit ****\n\n";

    running->out_file<<"Error: Overflow detected. Process terminated.\n\n";
    accounting();
    jobs.remove(running);
    completed.push_back(running);

    if(jobs.size()>0)
        next_running();
}//overflow()

void OS::page_fault()
{
    save_running();
    prog_out<<"\npage_fault\n";
    int victim = choose_victim();
    running->PFs++;
    running->table.entry[v.fault/8].modify = 0;
    running->table.entry[v.fault/8].valid = 1;
    running->table.entry[v.fault/8].frame = victim;
    inverted_table.entry[victim].pid = running->pid;
    inverted_table.entry[victim].page = (v.fault/8);
    inverted_table.entry[victim].valid = 1;

    v.load(running->o_file,(v.fault/8), victim);
    running->PFs++;
    if(v.sr.split.io_reg == 2)
        running->pc++;    

    running->ready_time = v.clock + 30;
    waitQ.push(running); //push onto back of waitQ

    if(readyQ.size() > 0)
        next_running();
    else{ //if nothing is ready then there is idle time, call scheduler
        scheduler();
        next_running();
    }
}

int OS::choose_victim()
{
    int victim = 0;
    PCB* victim_PCB;
    int victim_page;
    if(empty_frames.size() > 0){
        victim = empty_frames.front();
        frame_registersQ.push(victim);
        empty_frames.pop();
        v.all_limit += 8;
        if(victim >= (v.all_limit/8)){
            v.all_limit -= 8;    
            victim = choose_victim();
        } 
        return victim;
    } else { //LRU or FIFO
        if (replacement_alg){ //1 for LRU
            //search through VM's frame registers for oldest frame
            for (int i = 1; i < 32; i++)
                if(v.frame_registers[i] < v.frame_registers[victim])
                    victim = i;
            frame_registersQ.push(victim);
            v.frame_registers[victim] = v.clock;
        } else { //0 for FIFO
            victim = frame_registersQ.front();
            frame_registersQ.push(victim);
        }

        frame_registersQ.pop(); //for FIFO, but used in LRU to keep queue from growing
        //update valid bit of victim PCB
        list<PCB*>::iterator itr = jobs.begin();
        for(; itr != jobs.end(); itr++)
            if((*itr)->pid == inverted_table.entry[victim].pid){
                victim_PCB = (*itr);
                victim_page = inverted_table.entry[victim].page;
                break;
            }

        if(itr == jobs.end()){
            if(victim >= (v.all_limit/8))
                victim = choose_victim();
            return victim;
        }
        victim_PCB->table.entry[victim_page].valid = 0;

        //****check modify bit of victim****
        if(victim >= (v.all_limit/8)){
            victim = choose_victim();
            return victim;
        }
        if(victim_PCB->table.entry[victim_page].modify == 1)
            v.write_to_file(victim_PCB->o_file, victim_page, victim);

        return victim;
    }
}//choose_victim()

//writes process accounting info at end of .out file
void OS::accounting()
{
    double hits = (running->nPFs)/double(running->PFs + running->nPFs)*100;
    running->out_file << "\nProcess Information for " << running->process_name << "\n";
    running->out_file << "CPU Time: " << running->CPU_time << " ticks.\n";
    running->out_file << "Waiting Time: " << running->wait_time << " ticks.\n";
    running->out_file << "Turnaround Time: " << v.clock << " ticks.\n";
    running->out_file << "I/O Time: " << running->IO_time << " ticks.\n";
    running->out_file << "Largest Stack Size: " << running->lrg_st_size << endl;
    running->out_file << " Page Faults: " << running ->PFs <<endl;
    running->out_file << "Hit ratio: " << setprecision(4)<< hits << "%\n";

} //accounting()
 
//writes system accounting info at end of each .out file
void OS::accounting_final()
{
    list<PCB *>::iterator itr = completed.begin();
    int tot_CPU = 0, tot_PF = 0, tot_nPF = 0;
    double seconds = v.clock/1000.0;
    double CPU_util = (v.clock-idle_time)/double(v.clock);


    for (; itr != completed.end(); itr++){
	tot_CPU += (*itr)->CPU_time;
	tot_PF += (*itr)->PFs;
	tot_nPF += (*itr)->nPFs;
    }
    double hit_ratio = (tot_nPF)/double(tot_PF + tot_nPF)*100;
    double user_CPU = tot_CPU/double(v.clock);
    for (itr = completed.begin(); itr != completed.end(); itr++) {
        (*itr)->out_file << "\nSystem Information\n";
        (*itr)->out_file << "System Time: " << v.clock << endl;
	(*itr)->out_file << "System CPU Utilization: " << setprecision(4) << CPU_util*100 << "%\n";
        (*itr)->out_file << "User CPU Utilization: " << setprecision(4) << user_CPU*100 << "%\n";
        (*itr)->out_file << "Number of processes per second: " << setprecision(4) << completed.size()/seconds << endl;
        (*itr)->out_file << "Total page faults: " << tot_PF << endl;
	(*itr)->out_file << "Total hit ratio: " << setprecision(4) << hit_ratio << "%\n";
	(*itr)->out_file.close();
    }

    system("rm *.st");
} //accounting_final()
