/********************************************************************************
* CSE 460 - Spring 2016 - Phase II: Process Management
* Programmer:      Raymond Aceves, Caroline Hopkins
* File name:       main.cpp
* Date:            5/16/2016
* Description:     This is our main function. It simply creates an instance of OS
*                  and calls run().
********************************************************************************/
#include "OS.h"
#include <iostream>    //cout
#include <stdlib.h>    //exit()

using namespace std;

int main(int argc, char* argv[]) {
    OS os;
    if (argc != 2)
    {
        cout<<"Invalid number of arguments passed to OS.\n";
        exit(1);
    }
    os.run(argv[1]);
    return 0;
}


