/********************************************************************************
* CSE 460 - Spring 2016 - Phase III: Memory Management
* Programmer:      Raymond Aceves, Caroline Hopkins
* File name:       VirtualMachine.cpp
* Date:            6/13/2016
* Description:     Implementation for VirtualMachine.h. This  VirtualMachine
*                  has 256 memory locations that are 16 bits lon and there are 4
*                  general purpose registers. VirtualMachine will run a program,
*                  while receiving input from a .in file and output to a .out file
********************************************************************************/
#include "VirtualMachine.h"
#include <vector>
#include <string>
#include <iostream>
#include <stdlib.h> //exit()
#include <iomanip>  //set length of int when writing to o_file
using namespace std;

//16  bit format integer
class bit_converter
{
public:
    int last_16: 16;
};

//union used to convert from 32 bits to 16 bits
union converter {
    int bit_32;
    bit_converter bit_16;
};

//used to convert from 16 bit format to all 32 bits of the integer
int VirtualMachine::use_all_32(const int & in)
{
    converter convert;
    convert.bit_32 = 0;
    convert.bit_32 = in;
    int temp = convert.bit_16.last_16;
    return temp;
} //use_all_32()

//used to store a value in only the right most 16 bits
int VirtualMachine::store_in_16(const int & in)
{
    converter convert;
    convert.bit_32 = 0;
    convert.bit_16.last_16 = in;
    int temp = convert.bit_32;
    return temp;
} //store_in_16()

//constructer initializes registers and sets up the function pointer map
VirtualMachine::VirtualMachine()
{
    r = vector<int> (REG_FILE_SIZE);
    mem = vector<int> (MEM_SIZE);
    sp = MEM_SIZE;
    all_limit = pc = ir = sr.full = base = limit = clock = 0;
    largest_stack = 0;

    for(int i = 0; i < 32; i++){
        frame_registers.push_back(clock);
        limits.push_back(8);
    }
    current_page = 0;
   
//opcode map key format: (10*OP + I)  EX: addi = (10*OP + I) where OP = 2 and I = 1
    opcode[0] = &VirtualMachine::load;
    opcode[1] = &VirtualMachine::loadi;
    opcode[11] = &VirtualMachine::store;
    opcode[20] = &VirtualMachine::add;
    opcode[21] = &VirtualMachine::addi;
    opcode[30] = &VirtualMachine::addc;
    opcode[31] = &VirtualMachine::addci;
    opcode[40] = &VirtualMachine::sub;
    opcode[41] = &VirtualMachine::subi;
    opcode[50] = &VirtualMachine::subc;
    opcode[51] = &VirtualMachine::subci;
    opcode[60] = &VirtualMachine::and_f;
    opcode[61] = &VirtualMachine::andi;
    opcode[70] = &VirtualMachine::xor_f;
    opcode[71] = &VirtualMachine::xori;
    opcode[80] = &VirtualMachine::compl_f;
    opcode[90] = &VirtualMachine::shl;
    opcode[100] = &VirtualMachine::shla;
    opcode[110] = &VirtualMachine::shr;
    opcode[120] = &VirtualMachine::shra;
    opcode[130] = &VirtualMachine::compr;
    opcode[131] = &VirtualMachine::compri;
    opcode[140] = &VirtualMachine::getstat;
    opcode[150] = &VirtualMachine::putstat;
    opcode[161] = &VirtualMachine::jump;
    opcode[171] = &VirtualMachine::jumpl;
    opcode[181] = &VirtualMachine::jumpe;
    opcode[191] = &VirtualMachine::jumpg;
    opcode[201] = &VirtualMachine::call;
    opcode[210] = &VirtualMachine::return_f;
    opcode[220] = &VirtualMachine::read;
    opcode[230] = &VirtualMachine::write;
    opcode[240] = &VirtualMachine::halt;
    opcode[250] = &VirtualMachine::noop;
} //VirtualMachine()

//phase 3: add load specified page from object file to specified frame
void VirtualMachine::load(fstream & object_file, const int & page, const int & frame) //bring .o stream
{
    int index = frame*8;
    limits[frame] = 0;
    object_file.clear();
    object_file.seekg((page*6*8), ios::beg);

    while(object_file.good() and (index < ((frame+1)*8))){
        object_file>>mem[index++];
        limits[frame]++;
    }
} //load()

//writes modified page back to "disk"
void VirtualMachine::write_to_file(fstream & object_file, const int & page, const int & frame)
{
    int index = frame*8;
    object_file.clear();
    object_file.seekp((page*6*8), ios::beg);
    for(int i = 0; i < limits[frame]; i++){
        object_file << setfill('0') <<setw(5)<< mem[index++];
        if(i < (limits[frame]-1))
            object_file <<endl;
    }
    limits[frame] = 0;
}

//public function used in os.cpp to start VM
void VirtualMachine::run()
{
    sr.split.vm_return = 0;
    sr.split.io_reg = 0;
    sr.split.PF = 0;
    mem_ref = 0;

    //start the fetch, decode, execute cycle
    fetch_decode_execute();

} //run()

void VirtualMachine::fetch_decode_execute()
{
    int out_of_time = clock + TIME_SLICE;

    while((sr.split.vm_return == 0) and (sr.split.PF == 0) and (clock < out_of_time)){
        
        if(((pc)==(base+limit)) and ((limit%8) > 0)){//has reached internal fragmentation
            sr.split.vm_return = 2;
            break;
        }

        if(((pc)==base+limit) and ((limit%8) == 0)){//has reached next page
            if(TLB.entry[(current_page+1)].valid == 0){
                pc--;//will add one to running->pc later
                fault = (current_page+1)*8;
                sr.split.PF = 1;
                sr.split.io_reg = 2;
                break;
            } else {
                sr.split.PF = 0;
                mem_ref++;
                pc = TLB.entry[(current_page+1)].frame*8;
                current_page++;
                base = pc;
                limit = limits[pc/8];
            }
        }

        frame_registers[pc/8] = clock; //update this frame's clock for LRU
        ir = mem[pc++]; //fetch instruction from memory and increment pc
        decode();   //decode instruction
        execute();  //execute instruction
       //check if overflow bit has been set
        if(sr.split.overflow)
            break;
    }
} //fetch_decode_execute()


//decodes instruction by setting the following based on the instruction code
//sets OP, RD, I, RS, ADDR, and CONSt. stores them using only 16 bits
void VirtualMachine::decode()
{
    ins.binary_instruction = ir;
    OP = ins.uf.OP;
    RD = ins.uf.RD;
    I = ins.uf.I;
    RS = ins.uf.RS;
    ADDR = ins.af.ADDR;
    CONST = store_in_16(ins.cf.CONST);
} //decode()

//will execute the correct instruction based on OP and I
void VirtualMachine::execute()
{
    if(OP == 31){//if opcode is invalid
        sr.split.vm_return = 5;
        clock++;
    }else
        (this->*opcode[(OP*10)+I])();
} //execute()

//used by call() to push the VM status onto the stackS
void VirtualMachine::push()
{
    int logical_pc;
    if((pc%8) == 0)
        logical_pc = (current_page + 1)*8;
    else
        logical_pc = (current_page*8) + (pc%8);

    if(sp < (all_limit + 6)){
        frame_registers[(sp-6)/8] = clock;
        fault = logical_pc;
        sr.split.vm_return = 3;
        return;
    }
    
    mem[--sp] = logical_pc;
    mem[--sp] = r[0];
    mem[--sp] = r[1];
    mem[--sp] = r[2];
    mem[--sp] = r[3];
    mem[--sp] = sr.full;

    //check largest stack size and adjust if needed
    if((MEM_SIZE - sp) > largest_stack)
        largest_stack = MEM_SIZE - sp;
} //push()

//used by return() to pop stack and restore the VM status
void VirtualMachine::pop()
{
    if(sp >= MEM_SIZE){ //check that stack has a stored VM status
        sr.split.vm_return = 4;
        return;
    }

    sr.full = mem[sp++];
    r[3] = mem[sp++];
    r[2] = mem[sp++];
    r[1] = mem[sp++];
    r[0] = mem[sp++];
    pc = mem[sp++];
} //pop()

//used by the 8 add/sub functions
//checks for overflow and carry bit and sets the status register accordingly
int VirtualMachine::addition(const int & left, const int & right, const int & carry)
{
    int temp =store_in_16(left + right + carry);
    temp = use_all_32(temp);
    if ((left >= 0) and (right >= 0)) {
        sr.split.carry = 0;		//If both positive, carry bit must = 0
        if (temp < 0)
            sr.split.overflow = 1; 	// Set overflow bit when pos + pos = neg.
        else
            sr.split.overflow = 0;	// if pos + pos = pos, no overflow.
    } else if ((left < 0) and (right < 0)) {
        sr.split.carry = 1; 		//if both neg, there will always be carry bit.
        if ( temp >= 0)
            sr.split.overflow = 1;	//if neg + neg = pos or 0, overflow has occured.
        else
            sr.split.overflow = 0;	// no overflow.
    } else {
        if (((left+right+carry)&65536) != 0)
            sr.split.carry = 1;	// if temp[16] is a 1, set carry bit
        else
            sr.split.carry = 0;	// else, carry bit is zero.
        sr.split.overflow = 0;	//overflow cannot occur with pos + neg.
    }

    clock += 1; //all add/sub functions increase the clock value by 1
    return store_in_16(temp); //returns the result using only 16 bits
} //addition()

//loads value at memory location ADDR and stores into r[RD]
void VirtualMachine::load()
{
    int temp, temp_base, temp_limit;
    if(TLB.entry[ADDR/8].valid == 1){
        mem_ref++;
        sr.split.PF = 0;
        temp = (TLB.entry[ADDR/8].frame)*8 + (ADDR%8);
        temp_base = TLB.entry[ADDR/8].frame*8;
        temp_limit = limits[base/8];
        if(temp>=(temp_base+temp_limit)){
            sr.split.vm_return = 2;
            clock++;
        }else{
            r[RD] = mem[temp];
            clock += 4;
            frame_registers[temp/8] = clock;
        }
    } else {
        pc--;
        sr.split.PF = 1;
        fault = ADDR;
        sr.split.io_reg = 1; //set io_reg to 1 so that PC is decremented after page in loaded
    }

} //load()

//loads CONST into register r[RD]
void VirtualMachine::loadi()
{
    r[RD] = CONST;
    clock += 1;
} //loadi()

//stores the register r[RD] into memory location ADDR
void VirtualMachine::store()
{
    int temp, temp_base, temp_limit;
    if(TLB.entry[ADDR/8].valid == 1){
        mem_ref++;
        sr.split.PF = 0;
        temp = (TLB.entry[ADDR/8].frame)*8 + (ADDR%8);
        temp_base = TLB.entry[ADDR/8].frame*8;
        temp_limit = limits[base/8];
        if(temp>=(temp_base+temp_limit)){
            sr.split.vm_return = 2;
            clock++;
        }else{
            TLB.entry[ADDR/8].modify = 1;
            mem[temp] = r[RD];
            clock += 4;
            frame_registers[temp/8] = clock;
        }
    } else {
        pc--;
        sr.split.PF = 1;
        fault = ADDR;
        sr.split.io_reg = 1; //set io_reg to 1 so that PC is decremented after page in loaded
    }
} //store()

//adds r[RD] and r[RS]
void VirtualMachine::add()
{
    r[RD] = addition(use_all_32(r[RD]), use_all_32(r[RS]), 0);
} //add()

//adds CONST and r[RD]
void VirtualMachine::addi()
{
    r[RD] = addition(use_all_32(r[RD]), use_all_32(CONST), 0);
} //addi()

//add r[RD], r[RS], and carry  bit
void VirtualMachine::addc()
{
    r[RD] = addition(use_all_32(r[RD]), use_all_32(r[RS]), sr.split.carry);
} //addc()

//add r[RD], CONST, and carry bit
void VirtualMachine::addci()
{
    r[RD] = addition(use_all_32(r[RD]), use_all_32(CONST), sr.split.carry);
} //addci()

//subtract r[RS] from r[RD]
void VirtualMachine::sub()
{
    r[RD] = addition(use_all_32(r[RD]), -use_all_32(r[RS]), 0);
} //sub()

//subtract CONST from r[RD]
void VirtualMachine::subi()
{
    r[RD] = addition(use_all_32(r[RD]), -use_all_32(CONST), 0);
} //subi()

//subtract carry and r[RS] from r[RD]
void VirtualMachine::subc()
{
    r[RD] = addition(use_all_32(r[RD]), -use_all_32(r[RS]), -(sr.split.carry));
} //subc()

//subtract carry and CONST from r[RD]
void VirtualMachine::subci()
{
    r[RD] = addition(use_all_32(r[RD]), -use_all_32(CONST), -(sr.split.carry));
} //subci()

//binary and on r[RD] and r[RS] stored into r[RD]
void VirtualMachine::and_f()
{
    r[RD] = r[RD] & r[RS];
    clock += 1;
} //and_f()

//binary and on r[RD] and CONST stored into r[RD]
void VirtualMachine::andi()
{
    r[RD] = r[RD] & CONST;
    clock += 1;
} //andi()

//bitwise or of r[RD] and r[RS] stored into r[RD]
void VirtualMachine::xor_f()
{
    r[RD] = r[RD] ^ r[RS];
    clock += 1;
} //xor_f()

//bitwise or of r[RD] and CONST stored into r[RD]
void VirtualMachine::xori()
{
    r[RD] = r[RD] ^ CONST;
    clock += 1;
} //xori()

//finds bitwise compliment of r[RD]
void VirtualMachine::compl_f()
{
    r[RD] = ~r[RD]; //do a bit wise not on r[RD]
    r[RD] &= 65535; //change the left 16 bits of r[RD] back to all 0s
    clock += 1;     //add 1 to the clock
} //compl_f()

//binary shift left
void VirtualMachine::shl()
{
    int temp = r[RD];
    r[RD] = r[RD]<<1;

    if((r[RD]&65536) > 0){ //if bit[16] is 1
        sr.split.carry = 1;           // then set the carry to 1
        r[RD] &= 65535;    // and set bit[16] and above back to 0s
    } else
        sr.split.carry = 0;          //else set carry to 0
    clock += 1;
} //shl()

//shift left arithmetic
//will shift r[RD] left by one bit while preserving the sign bit
//will keep the left 16 bits of r[RD] as all 0s
//will set the carry bit equal to the sign bit
void VirtualMachine::shla()
{
    if((r[RD]>>15) == 1) {//if r[RD] is negative(bit 15 is 1) then do the following
        sr.split.carry = 1;          //set carry bit to 1
        r[RD] = r[RD]<<1; //bit-shift r[RD] left by 1 bit
        r[RD] |= 32768;   //set bit 15 to 1 to keep sign bit as negative
        r[RD] &= 65535;   //set bit 16 to 0
    } else {            //else r[RD] is >= to 0(bit 15 is 0) so do the following
        sr.split.carry = 0;         //set carry bit to 0
        r[RD] = r[RD]<<1; //bit-shift r[RD] left by 1 bit
        r[RD] &= 32767;   //set bit 15 to 0 to preserve sign bit as positive
    }

    clock += 1; //add 1 to the clock
}// shla()

//binary shift r[RD] right by 1

void VirtualMachine::shr()
{
    if((r[RD] & 1) > 0) //check what bit[0] will be, if it is 1
        sr.split.carry = 1;        //then set carry to 1
    else
        sr.split.carry = 0;       //else set carry to 0
    r[RD] = r[RD]>>1;
    clock += 1;
} //shr()

//shift right arithmaetic
//will shift r[RD] right by one bit while preserving the sign bit
//will keep the left 16 bits of r[RD] as all 0s
//will set the carry bit equal to the bit[0] before the shift
void VirtualMachine::shra()
{
    if((r[RD] & 1) > 0) //check what bit[0] will be, if it is 1
        sr.split.carry = 1;        //then set carry to 1
    else
        sr.split.carry = 0;       //else set carry to 0

    r[RD] = r[RD]>>1;

    if(r[RD] -16383 > 0) // preserve the sign bit
        r[RD] += 32768;

    clock += 1;
} //shra()

//if RD < RS, sr[3:1] = 100. if RD == RS, sr[3:1] = 010. else sr[3:1] = 001.
void VirtualMachine::compr()
{
    int RD_temp = use_all_32(r[RD]);
    int RS_temp = use_all_32(r[RS]);
    if (RD_temp < RS_temp) { //if r[RD] is less than r[RS]
        sr.split.less = 1;             //set less than bit to 1
        sr.split.equal = 0;
        sr.split.greater = 0;            //set greater and equal bits to 0
    } else if (RD_temp == RS_temp) { //else if they are equal
        sr.split.equal = 1;	//set equal bit to 1
        sr.split.less = 0;
        sr.split.greater = 0;       //set less and greater to 0
    } else {                //else r[RD] must be greater than r[RS]
        sr.split.greater = 1;	//set greater to 1
        sr.split.equal = 0;
        sr.split.less = 0;	//set equal and less to 0
    }
    clock += 1;
} //compr()

//same as compr() except checking r[RD] and CONST
void VirtualMachine::compri()
{
    int RD_temp = use_all_32(r[RD]);
    int C_temp = use_all_32(CONST);
    if (RD_temp < C_temp) {
        sr.split.less = 1;           
        sr.split.equal = 0;
        sr.split.greater = 0;	
    } else if (RD_temp == C_temp) {
        sr.split.equal = 1;
        sr.split.less = 0;       
        sr.split.greater = 0;
    } else {
        sr.split.greater = 1;        //set greater to 1
        sr.split.equal = 0;
        sr.split.less = 0;
    }
    clock += 1;
} //compri()

//sets r[RD] = status register
void VirtualMachine::getstat()
{
    r[RD] = sr.full;
    clock += 1;
} //getstat()

//sets status register = r[RD]
void VirtualMachine::putstat()
{
    sr.full = r[RD];
    clock += 1;
} //putstat()

//unconditional jump to program location by setting the PC to ADDR
void VirtualMachine::jump()
{
    if(TLB.entry[ADDR/8].valid == 1){
        mem_ref++;
        sr.split.PF = 0;
        pc = (TLB.entry[ADDR/8].frame)*8 + (ADDR%8);
        base = TLB.entry[ADDR/8].frame*8;
        limit = limits[base/8];
        current_page = ADDR/8;
        clock++;
        if(pc>=(base+limit))
            sr.split.vm_return = 2;
    } else {
        pc--;
        sr.split.PF = 1;
        fault = ADDR;
        sr.split.io_reg = 1;
    }

} //jump()

//jumps if status register is set to less than
void VirtualMachine::jumpl()
{
    if (sr.split.less)
        jump();     //if sr[3] = 1 (less than bit is 1) then jump()
    else
        clock += 1; //else do not jump but still use one clock cycle
} //jumpl()

//jumps if status register indicated equal to
void VirtualMachine::jumpe()
{
    if (sr.split.equal)
        jump();     //if sr[2] = 1 (equal bit is 1) then jump()
    else
        clock += 1; //else do not jump but still use one clock cycle
} //jumpe()

//jumps if status register indicates greater than
void VirtualMachine::jumpg()
{
    if (sr.split.greater)
        jump();     //if sr[1] = 1 (greater than bit is 1) then jump()
    else
        clock += 1; //else do not jump but still use one clock cycle
} //jumpg()

//function call jumps to function at memory ADDR by setting the PC value
void VirtualMachine::call()
{
    if(TLB.entry[ADDR/8].valid == 1){
        mem_ref++;
        sr.split.PF = 0;
        push();
        pc = (TLB.entry[ADDR/8].frame)*8 + (ADDR%8);
        base = TLB.entry[ADDR/8].frame*8;
        limit = limits[base/8];
        current_page = ADDR/8;
        if(pc>=(base+limit)){

            sr.split.vm_return = 2;
            clock++;
        }else{
            clock += 4;
        }
    } else {
        pc--;
        sr.split.PF = 1;
        fault = ADDR;
        sr.split.io_reg = 1;
    }
} //call()

//returns to previous location in the program and restores the VM status
void VirtualMachine::return_f()
{
    int previous_pc = pc-1;
    int temp_pc;
    pop(); //pop stack and restore vm status

    if(TLB.entry[pc/8].valid == 0){
        if((pc%8) == 0)
            current_page = (pc/8) - 1;
        else{
            current_page = pc/8;
        }
        push();
        sr.split.PF = 1;
        fault = pc;
        sr.split.io_reg = 1;
        pc = previous_pc;
    } else {
        mem_ref++;
        temp_pc = pc;
        pc = (TLB.entry[temp_pc/8].frame)*8 + (temp_pc%8);
        base = TLB.entry[temp_pc/8].frame*8;
        limit = limits[base/8];
        current_page = temp_pc/8;
      
        clock += 4;
    }
} //return_f()

//reads the net value from the .in file and stores it into r[RD]
void VirtualMachine::read()
{
    sr.split.vm_return = 6;
    sr.split.io_reg = RD;
    clock++;
}//read()

//write the value of r[RD] to the .out file
void VirtualMachine::write()
{
    sr.split.vm_return = 7;
    sr.split.io_reg = RD;
    clock++;
} //write()

//halt must end the fetch-decode-execute cycle
//set the veriable that is tested by the while loop to return false
void VirtualMachine::halt()
{
//cout<<pc-1<<endl;
    sr.split.vm_return = 1;    //in phase 2, the while loop stops when vm_return != 0
    clock += 1;
} //halt()

//noop does NOTHING except use up 1 clock cycle
void VirtualMachine::noop()
{
    clock += 1; //add 1 to the clock
} //noop()
