/********************************************************************************
* CSE 460 - Spring 2016 - Phase III: Memory Management
* Programmer:      Raymond Aceves, Caroline Hopkins
* File name:       page_table.h
* Date:            6/13/16
* Description:     The following is the implementation of the pabe table, TCB and
*                  the inverted page table. Note: the pagetable and TCB are the 
*                  same thing.
********************************************************************************/
#ifndef PAGE_TABLE_H
#define PAGE_TABLE_H

const unsigned TABLE_SIZE = 18;

class Entry
{
public:
    unsigned frame: 5;
    unsigned valid: 1;
    unsigned modify: 1;

    void operator=(Entry rhs){
        frame = rhs.frame;
        valid = rhs.valid;
        modify = rhs.modify;
    }
    
    Entry(){frame = 0; valid = 0; modify = 0;}
};

class PageTable
{
public:
    PageTable(){for(int i = 0; i < TABLE_SIZE; i++) entry[i] = Entry();}
private:
    void operator=(PageTable rhs){
        for(int i = 0; i < TABLE_SIZE; i++)
            entry[i] = rhs.entry[i];
    }
    Entry entry[TABLE_SIZE];
    friend class OS;
    friend class VirtualMachine;
};

class InvertedEntry
{
public:
    unsigned pid: 5;
    unsigned page: 5;
    unsigned valid: 1;
    unsigned modify: 1;
    
    InvertedEntry(){pid = 0; page = 0; valid = 0; modify = 0;}
};

class InvertedTable
{
public:
    InvertedTable(){for(int i = 0; i < 32; i++) entry[i] = InvertedEntry();}
private:
    InvertedEntry entry[32];
    friend class OS;
};

#endif
